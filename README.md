# Saccamuse

![Saccamuse song view](./assets/pics/list_view.jpg)

## Background & Presentation

As a swing dance DJ (or any partner dance DJ), where beatmatching is not a consideration, there is a multitude of possible tools to sort and play music.

Personally, I have been using the following setup:

- A (linux) laptop + an external USB sound card
- A basic music player, which plays the songs for the dancefloor through the USB sound card. I have been using [Clementine Music Player](https://www.clementine-player.org/).
- A software which can display my database of all songs, with the ability to display tags, that I use to preview music live through the native soundcard of the laptop to the headphones, but also that I use to tag my music beforehand.

Historically, I was using [TagSpaces](https://www.tagspaces.org/) to tag my music by saving the tags directly in the file names. The reasoning is that the support of ID3 tags varies a lot depending on the software. Some softwares supports custom fields, some don't and it's, therefore unreliable. Even though more limited, using tags encoded in filenames allows for emergency DJ sessions from a phone or USB key with any kind of music player. It's easy to synchronize among devices to create multiple backup of the music database (to avoid hundreds of hours going to waste).

Unfortunately, starting from version 3.0, Tagspaces has started to have performances issues when manipulating a large number of tags. Therefore, I have decided to create my own app as a replacement. The tag file should still be compatible with Tagspaces.


## Goals & Non-goals of Saccamuse

The main goals of Saccamuse are:

- Displaying list of audio files with Tagspaces-like tags, where the tags are highlisghted with colors. It helps during DJ session to find music fast.
- Allows listening to the song by opening the VLC player.
- Allows to create/edit tag groups and tags
- Allows to add/remove tags from audio files
- No metadata stored in a database, which means full portability
- Tags are stored in a JSON file (easy export)

Non-goals:

- Saccamuse is not meant to be a universal tagging and previewing system like Tagspaces.
- There won't be any smartphone release

Optional goals if more people are interested in the project:

- Prettier UI and better UX
- Embedded music player (instead of VLC)
- Releases for MacOS
- Tagging for other file types and file previewing with external app.
- Mass tagging / tagging multiple files at once.

## Installation

### Download binary

Go to the [release page](https://gitlab.com/lcoutu/saccamuse/-/releases) and download the package from the latest release for your OS.

There is no release for MacOS as I don't have a mac and cross-compilation is hard. However, you can compile the software from source (seel below).

Install [VLC player](https://www.videolan.org/vlc/).

### Compile from source

Follow the instructions:

- [Install rust on your computer](https://www.rust-lang.org/learn/get-started)
- Clone the repo `git clone https://gitlab.com/lcoutu/saccamuse.git`
- Open a terminal and go to the folder `cd saccamuse`
- Compile the software: `cargo build --release`
- There should be a binary in `saccamuse/target` that you can execute.

## Manual

Saccamus offers all the feature that I need, but the interface was not meant to be clear for external user.

Open Saccamuse by executing the binary. The configuration of the app will be stored in your home folder:

- On linux in `/home/[user]/.config/saccamuse/`
- On Windows in `C:/Users/[user]/AppData/Roaming/saccamuse`

In the folder you will find the .json file with all your tags. This file is edited every time you add new tags to your collection.

### Folder Selection

You are welcomed with the folder selection screen:
![Saccamuse song folder selection](assets/pics/select_folder.jpg)

- A: Enter the path of the folder that you want to add or use `Select Folder` to open the folder selection pop-up
- B: Click on `Add folder to list` to add the folder as one of the selectable folders. Alternatively, you can remove from the list by clicking on `Remove folder from list`
- C: Select the folder with all the songs that you want to open
- D: Click on `Finish` to open the folder

### Song view

This is the main interface of Saccamuse. It displays all the audio files in the folder.
![Saccamuse song view annotated](assets/pics/list_view_annoted.jpg)

- A: Double click on a song name to play the song with VLC
- B: Double click on of the tag to open the tag editor

### Tag Editor

This is the windows where we can edit tags and the song name. **Changes are applied when the editor is closed**, the file name is then edited with the changes.

![Saccamuse tag editor annotated](assets/pics/tag_editor.jpg)

- A: You can change the name of the song.
- B: you can maniuplate existing tags:
  - Left click: move the tag to the left
  - Right click: move the tag to the right
  - Middle click: delete the tag
- C: This editing box is used multiple cases (see below)
- D: **Edit tags**
  - **Add tag**:
    - Select the tag group
    - Click "Add tag".
    - __It will add the tag to the group and to the file. If the tag already exists, it will change the group to which the tag belongs.__
  - **Delete tag**
    - Simply enter the name of the tag and select "Del. tag from database".
    - The tag will be removed from the database, but not from the filename ! So the tag will still exist in the "Auto tag" group, which gathers all the tags which are not present in an other tag group. As long as one of the file contains that tag, it will be present in the "Auto tag group".
- E: **Edit tag groups**:
  - **Create tag group**:
    - Enter the name of the new group
    - Select the color of the new group. On the left is the text color, on the right the background color. All tag of the group will have the same color (you could manually edit the color in the file afterwards if you want an exception).
    - Click on "Add group". The group should be in the group list.
  - **Delete group**:
    - Enter the name of the group to delete.
    - Click on "Delete group".
    - All the tags of the group will be added to the default group "Auto tag". After a restart of the software, all the existing tag will take the color of the "Auto tag group". It's kind of a bug, but this is an edge use case for now.
  - F: **Add an existing tag to the song**
    - Just click on one of the tags in one of the menu.
  - G: **Apply / cancel all the changes**
    - Click on "Save" to apply all the changes you have made, the file will be edited and the tag collection updated and savex
    - Click on "Cancel" and nothing should happen.

That's it.

