#[macro_use]
extern crate gstuff;
mod app;
pub mod example_scripts;
pub mod parsing;
pub use app::SaccamuseApp;
pub mod config;
pub mod data;
pub mod file_watching;
