use std::path::PathBuf;

use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};

#[cfg(debug_assertions)]
lazy_static! {
    pub static ref APP_NAME: String = "saccamuse_debug".into();
}

#[cfg(not(debug_assertions))]
lazy_static! {
    pub static ref APP_NAME: String = "saccamuse".into();
}

pub fn launch_config_selector() {
    let native_options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_inner_size([400.0, 300.0])
            .with_icon(
                // NOTE: Adding an icon is optional
                eframe::icon_data::from_png_bytes(&include_bytes!("../assets/icon-256.png")[..])
                    .expect("Failed to load icon"),
            )
            .with_title("Select folder".to_string()),
        ..Default::default()
    };
    let _ = eframe::run_native(
        "saccamuse_config_selector",
        native_options,
        Box::new(|cc| Ok(Box::new(ConfigApp::new(cc)))),
    );
}

#[derive(Debug, Clone)]
pub struct ConfigApp {
    selected_folder: Option<usize>,
    config: SaccamuseConfig,
    new_path_string: String,
}

impl Default for ConfigApp {
    fn default() -> Self {
        let loaded_config: SaccamuseConfig = confy::load(&APP_NAME, None).unwrap();

        let path_index = loaded_config
            .paths
            .iter()
            .position(|x| *x == loaded_config.selected_path);

        Self {
            selected_folder: path_index,
            new_path_string: "".to_string(),
            config: loaded_config,
        }
    }
}

impl ConfigApp {
    pub fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        ConfigApp::default()
    }
}

impl eframe::App for ConfigApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Edit list of available folders:");
            ui.horizontal(|ui| {
                ui.add(
                    egui::TextEdit::singleline(&mut self.new_path_string).hint_text("New Folder"),
                );
                if ui.button("Select folder").clicked() {
                    if let Some(path) = rfd::FileDialog::new().pick_folder() {
                        self.new_path_string = path.to_str().unwrap().into();
                    }
                }
            });

            ui.horizontal(|ui| {
                if ui.button("Add folder to list").clicked() {
                    self.config.add_path(self.new_path_string.clone());
                }
                if ui.button("Remove folder from list").clicked() {
                    self.config.remove_path(self.new_path_string.clone());
                }
            });
            ui.heading("Select folder to open:");
            let select_ndx: usize = match self.selected_folder {
                Some(x) => {
                    if x < self.config.paths.len() {
                        x
                    } else {
                        0
                    }
                }
                None => 0,
            };
            egui::ComboBox::from_label("")
                .selected_text(self.config.paths[select_ndx].to_str().unwrap())
                .show_ui(ui, |ui| {
                    for i in 0..self.config.paths.len() {
                        let value = ui.selectable_value(
                            &mut &self.config.paths[i],
                            &self.config.paths[self.selected_folder.unwrap_or(0)],
                            self.config.paths[i].to_str().unwrap(),
                        );
                        if value.clicked() {
                            self.selected_folder = Some(i);
                        }
                    }
                });
            ui.separator();
            if ui.button("Finish").clicked() {
                ctx.send_viewport_cmd(egui::ViewportCommand::Close);
            }
        });
    }

    fn on_exit(&mut self, _gl: Option<&eframe::glow::Context>) {
        self.config.selected_path = match self.selected_folder {
            Some(s1) => self.config.paths[s1].clone(),
            None => self.config.paths[0].clone(),
        };
        confy::store(&APP_NAME, None, &self.config)
            .unwrap_or_else(|_| log::error!("Could not save config."));
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct SaccamuseConfig {
    paths: Vec<PathBuf>,
    selected_path: PathBuf,
    tag_file_path: PathBuf,
}

impl Default for SaccamuseConfig {
    fn default() -> Self {
        let mut tmp_path: PathBuf = confy::get_configuration_file_path(&APP_NAME, None)
            .unwrap()
            .parent()
            .unwrap()
            .to_path_buf();
        tmp_path.push("tags.json");

        // let mut def_path = PathBuf::from("/");

        // #[cfg(target_os = "windows")]
        // {
        //     def_path = PathBuf::from("c:\\");
        // }

        SaccamuseConfig {
            paths: vec![PathBuf::default()],
            selected_path: PathBuf::default(),
            tag_file_path: tmp_path,
        }
    }
}

impl SaccamuseConfig {
    fn add_path(&mut self, path: String) {
        let path: PathBuf = path.into();
        // let res = self.paths.push(path.into());

        if path.exists() {
            self.paths.push(path)
        } else {
            let tmp_path = path.as_os_str().to_str().unwrap();
            log::warn! {"{tmp_path} does not exists"}
        }
    }
    fn remove_path(&mut self, path: String) {
        let tmp_path: PathBuf = path.into();
        self.paths = self
            .paths
            .iter()
            .filter(|x| **x != tmp_path)
            .cloned()
            .collect::<Vec<PathBuf>>();
    }
}

pub fn get_folder_from_config() -> (PathBuf, PathBuf) {
    let loaded_config: SaccamuseConfig = confy::load(&APP_NAME, None).unwrap();
    let folder_path = loaded_config.selected_path.as_os_str().to_str().unwrap();
    let tag_path = loaded_config.tag_file_path.as_os_str().to_str().unwrap();

    (folder_path.into(), tag_path.into())
}
