#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))] // Forbid warnings in release builds
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] //Hide console window in release builds on Windows, this blocks stdout.

// #[macro_use]
// extern crate gstuff;

use saccamuse::config::get_folder_from_config;
use saccamuse::config::launch_config_selector;
use saccamuse::config::APP_NAME;
use saccamuse::data::DataCollection;
use saccamuse::file_watching::FileWatcher;
use saccamuse::parsing::create_tag_hashmaps;
use saccamuse::parsing::parse_folder;
use saccamuse::parsing::parse_tag_file;
use saccamuse::parsing::SeAllTags;

use std::fs::File;
use std::fs::{self};
use std::io::Write;

fn main() -> eframe::Result {
    launch_config_selector();
    let (folder_to_open, tag_file_path) = get_folder_from_config();

    tracing_subscriber::fmt::init();

    if !tag_file_path.exists() {
        let empty_tag_file = SeAllTags::default();
        let json_str = serde_json::to_string_pretty(&empty_tag_file).unwrap();
        let mut file = File::create(tag_file_path.clone()).expect("Could not create file!");
        let _ = file.write_all(json_str.as_bytes());
    }
    let json_data = fs::read_to_string(tag_file_path.clone()).expect("Unable to read file");
    let file_tags = parse_tag_file(&json_data).unwrap();

    let mut data_collection = DataCollection::new(folder_to_open.clone());
    let (tag_groups, tag_map) = create_tag_hashmaps(file_tags.clone());

    let file_names = parse_folder(folder_to_open.clone());

    let now = std::time::Instant::now();
    data_collection.set_maps(tag_groups, tag_map);
    data_collection.parse_file_names(file_names, folder_to_open.clone());
    let elapsed = now.elapsed();
    println!(
        "{} {} {:#?}",
        { "\x1b[38;5;92m⮚\x1b[0m" },
        { "\x1b[38;5;92m Files parsed in:\x1b[0m" },
        { elapsed }
    );

    let file_watcher = FileWatcher::launch_watcher(&folder_to_open);
    let folder_name = folder_to_open.file_name().unwrap().to_str().unwrap();
    let title_name = format!("{folder_name} - Saccamuse");

    let native_options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_inner_size([400.0, 300.0])
            .with_min_inner_size([300.0, 220.0])
            .with_icon(
                // NOTE: Adding an icon is optional
                eframe::icon_data::from_png_bytes(&include_bytes!("../assets/icon-256.png")[..])
                    .expect("Failed to load icon"),
            )
            .with_title(title_name),
        ..Default::default()
    };

    eframe::run_native(
        &APP_NAME,
        native_options,
        Box::new(|cc| {
            Ok(Box::new(saccamuse::SaccamuseApp::new(
                cc,
                data_collection,
                file_watcher,
                file_tags,
                tag_file_path,
            )))
        }),
    )
}
