use std::{
    path::{Path, PathBuf},
    sync::{
        mpsc::{Receiver, Sender},
        Arc, Mutex,
    },
    thread::{self, JoinHandle},
    time::Duration,
};

#[cfg(not(target_os = "windows"))]
use notify_debouncer_full::{
    new_debouncer,
    notify::{EventKind, INotifyWatcher, RecursiveMode, Watcher},
    DebouncedEvent, Debouncer, FileIdMap,
};

#[cfg(target_os = "windows")]
use notify_debouncer_full::{
    new_debouncer,
    notify::{EventKind, ReadDirectoryChangesWatcher, RecursiveMode, Watcher},
    DebouncedEvent, Debouncer, FileIdMap,
};

const DEBOUNCE_PERIOD: u64 = 100; //ms

#[cfg(not(target_os = "windows"))]
#[derive(Debug)]
pub struct FileWatcher {
    pub path: PathBuf,
    pub new_change: Arc<Mutex<bool>>,
    pub watcher_sender:
        Option<Sender<Result<Vec<DebouncedEvent>, Vec<notify_debouncer_full::notify::Error>>>>,
    pub thread_handle: JoinHandle<()>,
    _watcher: Debouncer<INotifyWatcher, FileIdMap>,
}

#[cfg(target_os = "windows")]
#[derive(Debug)]
pub struct FileWatcher {
    pub path: PathBuf,
    pub new_change: Arc<Mutex<bool>>,
    pub watcher_sender:
        Option<Sender<Result<Vec<DebouncedEvent>, Vec<notify_debouncer_full::notify::Error>>>>,
    pub thread_handle: JoinHandle<()>,
    _watcher: Debouncer<ReadDirectoryChangesWatcher, FileIdMap>,
}

impl FileWatcher {
    pub fn launch_watcher(path: &Path) -> FileWatcher {
        let change_detected_bool = Arc::new(Mutex::new(false));

        let change_detected_bool_2 = Arc::clone(&change_detected_bool);

        let (tx, rx) = std::sync::mpsc::channel();
        let tx2: Sender<Result<Vec<DebouncedEvent>, Vec<notify_debouncer_full::notify::Error>>> =
            tx.clone();

        let mut debouncer =
            new_debouncer(Duration::from_millis(DEBOUNCE_PERIOD), None, tx).unwrap();

        debouncer
            .watcher()
            .watch(Path::new(&path), RecursiveMode::Recursive)
            .unwrap();

        let handle = create_filewatcher_thread(rx, change_detected_bool_2);

        FileWatcher {
            path: path.to_path_buf(),
            new_change: change_detected_bool,
            watcher_sender: Some(tx2),
            thread_handle: handle,
            _watcher: debouncer,
        }
    }

    pub fn stop_watcher(&mut self) {
        // acquire the sender to the file watcher:
        let watcher_sender = self.watcher_sender.take();
        // drop it to stop the watcher:
        drop(watcher_sender)
    }
}

fn create_filewatcher_thread(
    rx: Receiver<Result<Vec<DebouncedEvent>, Vec<notify_debouncer_full::notify::Error>>>,
    change_detect: Arc<Mutex<bool>>,
) -> std::thread::JoinHandle<()> {
    // thread::sleep(time::Duration::from_secs(15));
    thread::spawn(move || {
        for result in rx {
            match result {
                Ok(events) => events.iter().for_each(|event| {
                    log::debug!("Event {event:?}");
                    match event.kind {
                        // In case a file is modified, notify it:
                        EventKind::Create(_) | EventKind::Modify(_) | EventKind::Remove(_) => {
                            let mut detect_bool = change_detect.lock().unwrap();
                            *detect_bool = true;
                            drop(detect_bool);
                        }
                        _ => (),
                    }
                }),
                Err(error) => log::info!("Error {error:?}"),
            }
        }
    })
}
