use std::{collections::HashMap, fs, path::PathBuf};

use egui::Color32;
use hex_color::HexColor;
use rayon::iter::IntoParallelRefIterator;

use crate::{
    app::TagEditor,
    parsing::{
        parse_file_name, split_artist_song, FileType, ALLOWED_FORMATS, DEFAULT_BG_COLOR,
        DEFAULT_TAG_GROUP, DEFAULT_TEXT_COLOR,
    },
};

pub type GroupHashmap = HashMap<String, TagGroup>;

#[derive(Default, Debug)]
pub struct DataCollection {
    pub tag_map: HashMap<String, Tag>,
    pub song_entries: HashMap<String, SongEntry>,
    pub group_map: GroupHashmap,
    pub row_entries: HashMap<usize, String>,
    pub row_entries_by_bpm: HashMap<usize, String>,
    pub group_tag_map: HashMap<String, Vec<String>>,
    pub root_path: PathBuf,
}

impl DataCollection {
    pub fn new(root_path: PathBuf) -> DataCollection {
        DataCollection {
            tag_map: HashMap::new(),
            song_entries: HashMap::new(),
            group_map: HashMap::new(),
            row_entries: HashMap::new(),
            row_entries_by_bpm: HashMap::new(),
            group_tag_map: HashMap::new(),
            root_path,
        }
    }

    pub fn set_maps(&mut self, tag_groups: GroupHashmap, tag_map: HashMap<String, Tag>) {
        self.tag_map = tag_map;
        self.group_map = tag_groups;

        for group_name in self.group_map.keys() {
            let mut tmp_group_tags: Vec<String> = vec![];
            for tag in self.tag_map.values() {
                if tag.group == *group_name {
                    tmp_group_tags.push(tag.title.clone());
                }
            }
            tmp_group_tags.sort();
            self.group_tag_map
                .insert(group_name.clone(), tmp_group_tags);
        }

        for tg in self.group_tag_map.values_mut() {
            tg.sort()
        }
    }

    pub fn filtered_row_entries(&self, song_filter: String) -> Vec<String> {
        let res = self
            .row_entries
            .iter()
            .filter(|(_, val)| val.to_lowercase().contains(&song_filter.to_lowercase()))
            .map(|(_, val)| val.clone())
            .collect();
        res
    }
    pub fn filtered_row_entries_by_bpm(&self, song_filter: String) -> Vec<String> {
        let res = self
            .row_entries_by_bpm
            .iter()
            .filter(|(_, val)| val.to_lowercase().contains(&song_filter.to_lowercase()))
            .map(|(_, val)| val.clone())
            .collect();
        res
    }

    pub fn delete_group(&mut self, group_name: String) -> bool {
        let existing_group = self.group_map.get(&group_name);
        let group_exists: bool = existing_group.is_some();
        if !group_exists {
            return false;
        }

        if let Some(s2) = existing_group {
            let group = s2.title.clone();
            self.group_map.remove(&group);
            self.group_tag_map.remove(&group);
        }
        // self.add_group(
        //     DEFAULT_TAG_GROUP.clone(),
        //     Color32::from_rgb(
        //         DEFAULT_TEXT_COLOR.r,
        //         DEFAULT_TEXT_COLOR.g,
        //         DEFAULT_TEXT_COLOR.b,
        //     ),
        //     Color32::from_rgb(DEFAULT_BG_COLOR.r, DEFAULT_BG_COLOR.g, DEFAULT_BG_COLOR.b),
        // );
        true
    }
    pub fn add_group(
        &mut self,
        group_name: String,
        text_color: Color32,
        bg_color: Color32,
    ) -> bool {
        // Check if the tag does not exists yet:
        let existing_group = self.group_tag_map.get(&group_name);
        if existing_group.is_some() {
            return true;
        }

        let bg_hex_col = HexColor::parse(&bg_color.to_hex()).unwrap();
        let text_hex_col = HexColor::parse(&text_color.to_hex()).unwrap();
        self.group_map.insert(
            group_name.clone(),
            TagGroup {
                title: group_name.clone(),
                bg_color: Some(bg_hex_col),
                text_color: Some(text_hex_col),
            },
        );
        self.group_tag_map.insert(group_name, vec![]);
        false
    }

    pub fn add_tag(&mut self, tag_name: String, group_name: Option<&String>) {
        // Check if the tag does not exists yet:
        let existing_tag = self.tag_map.get(&tag_name);
        let tag_exists: bool = existing_tag.is_some();
        let mut tag_to_remove: Option<String> = None;
        if let Some(x) = existing_tag {
            if group_name.is_some()
                && x.group == *DEFAULT_TAG_GROUP
                && *group_name.unwrap() != *DEFAULT_TAG_GROUP
            {
                tag_to_remove = Some(x.title.clone());
                // self.tag_map.remove(&x.title.clone());
                let auto_added_tags = self
                    .group_tag_map
                    .get_mut(&DEFAULT_TAG_GROUP.clone())
                    .unwrap();
                *auto_added_tags = auto_added_tags
                    .iter()
                    .filter(|x| **x != tag_name)
                    .cloned()
                    .collect::<Vec<String>>();
            }
        }
        if let Some(s2) = tag_to_remove {
            self.tag_map.remove(&s2);
        }

        if tag_exists && group_name.is_none() {
            log::warn!("Tag {tag_name} already exists");
            return;
        }

        let bg_color = match group_name {
            Some(s1) => self
                .group_map
                .get(s1)
                .unwrap()
                .bg_color
                .unwrap_or_else(|| *DEFAULT_BG_COLOR),
            None => *DEFAULT_BG_COLOR,
        };
        let text_color = match group_name {
            Some(s1) => self
                .group_map
                .get(s1)
                .unwrap()
                .text_color
                .unwrap_or_else(|| *DEFAULT_TEXT_COLOR),
            None => *DEFAULT_TEXT_COLOR,
        };

        let tmp_group_name = match group_name {
            Some(s1) => s1.clone(),
            None => DEFAULT_TAG_GROUP.clone(),
        };

        let tmp_tag = Tag {
            title: tag_name.clone(),
            bg_color,
            text_color,
            group: tmp_group_name.clone(),
        };
        self.tag_map.insert(tag_name.clone(), tmp_tag);

        let tmp_tag_group = self.group_tag_map.get_mut(&tmp_group_name);
        match tmp_tag_group {
            Some(s1) => {
                s1.push(tag_name);
                // s1.sort();
            }
            None => {
                self.group_tag_map
                    .insert(tmp_group_name.clone(), vec![tag_name]);
            }
        }
    }
    pub fn remove_tag(&mut self, tag_name: String) {
        // Check if the tag exists:
        let existing_tag = self.tag_map.get(&tag_name);
        let tag_exists: bool = existing_tag.is_some();
        if !tag_exists {
            return;
        }

        if let Some(s2) = existing_tag {
            let group = &s2.group;

            let tmp_group = self.group_tag_map.get(group);
            if let Some(s3) = tmp_group {
                let filtered_list = s3
                    .iter()
                    .filter(|x| **x != tag_name)
                    .cloned()
                    .collect::<Vec<String>>();
                self.group_tag_map.insert(group.to_string(), filtered_list);
            }
            self.tag_map.remove(&tag_name);
        }
    }

    pub fn sort_tags(&mut self) {
        self.group_tag_map.iter_mut().for_each(|(_, tags)| {
            tags.sort_by_key(|a| a.to_lowercase());
        });
    }

    // pub fn data_len(&self) -> usize {
    //     self.song_entries.len()
    // }

    /// Parse the names and generate the structure to display and manipulate tags.
    ///
    /// # Arguments
    ///
    /// * `file_names` - a list of files names
    pub fn parse_file_names(&mut self, file_names: Vec<String>, root_path: PathBuf) {
        self.song_entries = HashMap::new();
        self.row_entries = HashMap::new();
        self.row_entries_by_bpm = HashMap::new();

        let mut tmp_bpm_song_vec: Vec<(usize, SongEntry)> = vec![];

        for file_name in file_names.iter() {
            let parse_res = parse_file_name(file_name);
            match parse_res.extension.clone() {
                Some(x) => {
                    if !ALLOWED_FORMATS.contains(&String::as_str(&x)) {
                        continue;
                    }
                }
                None => continue,
            }
            let mut entry_tags: Vec<String> = vec![];
            let entry_tag_opt: Option<Vec<String>>;
            let mut tmp_bpm: usize = 0;
            if let Some(x) = parse_res.tags {
                for tmp_tag in x.clone() {
                    // Check if it's a number (=BPM)
                    if let Ok(s1) = tmp_tag.parse::<usize>() {
                        tmp_bpm = s1
                    }

                    let matched_tag = self.tag_map.get(&tmp_tag);
                    match matched_tag {
                        // The tag already exist
                        Some(_) => {}
                        // The tag does not exist, we add it to the hashmap first
                        None => {
                            self.add_tag(tmp_tag.clone(), None);
                        }
                    }
                    entry_tags.push(tmp_tag.clone());
                }
                entry_tag_opt = Some(entry_tags);
            } else {
                entry_tag_opt = Some(vec![]);
            }

            let mut full_path = root_path.clone();
            full_path.push(file_name);
            let tmp_file_entry = SongEntry {
                artist: parse_res.artist,
                song_name: parse_res.song,
                extension: parse_res.extension,
                file_type: FileType::Audio,
                tags: entry_tag_opt, // At the first pass, we keep the reference empty
                root_path: root_path.clone(),
                full_path,
            };

            self.row_entries
                .insert(self.row_entries.len(), tmp_file_entry.hash_string());
            self.song_entries
                .insert(tmp_file_entry.hash_string(), tmp_file_entry.clone());
            tmp_bpm_song_vec.push((tmp_bpm, tmp_file_entry))
        }

        tmp_bpm_song_vec.sort_by(|a, b| {
            if a.0 == b.0 {
                a.1.hash_string().partial_cmp(&b.1.hash_string()).unwrap()
            } else {
                a.0.partial_cmp(&b.0).unwrap()
            }
        });

        for (i, (_, song_name)) in tmp_bpm_song_vec.iter().enumerate() {
            self.row_entries_by_bpm.insert(i, song_name.hash_string());
        }
    }

    pub fn process_tag_editor(&mut self, tag_editor: TagEditor) {
        // WIP:  somehow it's the hash which is returned instead of the song name...
        let old_song_name = tag_editor.song.full_path.clone();

        if let Some(s1) = tag_editor.output {
            if s1.delete_song {
                match fs::remove_file(old_song_name.clone()) {
                    Ok(_) => (),
                    Err(s1) => {
                        log::error!("{}. File: {:?}", s1, old_song_name.to_str())
                    }
                }
                return;
            }

            let mut all_parts: Vec<String> = vec![];
            let original_song = tag_editor.song;

            // Set the full name of the song (artist + name):
            if let Some(s2) = s1.full_song_name {
                let (artist, song) = split_artist_song(&s2);
                if let Some(s3) = artist {
                    all_parts.push(s3);
                    all_parts.push(" - ".into());
                    all_parts.push(song);
                }
            } else {
                all_parts.push(original_song.full_name());
            }

            // Assemble the tags:
            let tmp_tags: Option<Vec<String>> = match s1.tags {
                Some(s3) => Some(s3),
                None => original_song.tags,
            };
            if let Some(s4) = tmp_tags {
                all_parts.push(" [".into());
                for tag in s4 {
                    all_parts.push(tag);
                    all_parts.push(" ".into());
                }
                all_parts.pop();
                all_parts.push("]".into());
            }

            all_parts.push(".".into());
            all_parts.push(original_song.extension.unwrap());
            let mut res_path: PathBuf = original_song.root_path;
            res_path.push(all_parts.join(""));

            match fs::rename(old_song_name, res_path.clone()) {
                Ok(_) => (),
                Err(s1) => {
                    log::error!("{}. File: {:?}", s1, res_path.to_str())
                }
            }
        }
    }

    pub fn get_tag_groups_list(&self) -> Vec<String> {
        let mut res: Vec<String> = self
            .group_map
            .keys()
            .collect::<Vec<&String>>()
            .iter()
            .map(|&x| x.clone())
            .collect();
        res.sort();
        res
    }
}

#[derive(Default, Debug, Clone)]
pub struct TagGroup {
    pub title: String,
    pub bg_color: Option<HexColor>,
    pub text_color: Option<HexColor>,
}

impl TagGroup {}

#[derive(Default, Debug, Clone)]
pub struct Tag {
    pub title: String,
    pub bg_color: HexColor,
    pub text_color: HexColor,
    pub group: String,
}

impl Tag {}

#[derive(Debug, Clone)]
pub struct SongEntry {
    pub artist: Option<String>,
    pub song_name: String,
    pub extension: Option<String>,
    pub file_type: FileType,
    pub tags: Option<Vec<String>>,
    pub root_path: PathBuf,
    pub full_path: PathBuf,
}

impl SongEntry {
    pub fn hash_string(&self) -> String {
        let mut res = match &self.artist {
            Some(x) => {
                let mut res = x.clone();
                res.push_str(" - ");
                res.push_str(&self.song_name);
                res
            }
            None => self.song_name.clone(),
        };

        // We also use the tags as hash, it does not matter that the tags are updated afterwards,
        // what matters is that the hash is unique. It will be rebuilts at startup anyway.
        if let Some(s1) = self.tags.clone() {
            let tmp_str = s1.join("");
            res = [res, tmp_str].join("")
        }
        res
    }

    pub fn full_name(&self) -> String {
        match &self.artist {
            Some(x) => {
                let mut res = x.clone();
                res.push_str(" - ");
                res.push_str(&self.song_name);
                res
            }
            None => self.song_name.clone(),
        }
    }
}
