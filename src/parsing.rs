use glob::glob;
use hex_color::HexColor;

use serde::{Deserialize, Serialize};
use serde_json::Result;
use uuid::Uuid;

use std::collections::HashMap;
use std::str::FromStr;

use winnow::error::ErrMode;

use winnow::error::InputError;

use winnow::token::take;
use winnow::token::take_until;
use winnow::IResult;

use winnow::Parser;

use hex_color::Display;
use lazy_static::lazy_static;
use std::{
    fs::{self},
    path::PathBuf,
};

use crate::config::APP_NAME;
use crate::data::DataCollection;
use crate::data::GroupHashmap;
use crate::data::Tag;
use crate::data::TagGroup;

lazy_static! {
    pub static ref DEFAULT_BG_COLOR: HexColor = HexColor::parse("#008000").unwrap();
    pub static ref DEFAULT_TEXT_COLOR: HexColor = HexColor::parse("#ffffff").unwrap();
    pub static ref DEFAULT_TAG_GROUP: String = "Auto tag".into();
}

pub const ALLOWED_FORMATS: [&str; 4] = ["ogg", "mp3", "flac", "wav"];

// Structure containuing all deserialized tags
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SeAllTags {
    #[serde(rename = "appName")]
    pub app_name: String,
    #[serde(rename = "appVersion")]
    pub app_version: String,
    #[serde(rename = "settingsVersion")]
    pub settings_version: u8,
    #[serde(rename = "tagGroups")]
    pub tag_groups: Vec<SeTagGroup>,
}

impl Default for SeAllTags {
    fn default() -> Self {
        Self {
            app_name: APP_NAME.to_string(),
            app_version: "6.0.0".to_owned(),
            settings_version: 3,
            tag_groups: vec![],
        }
    }
}

// Structure with deserialized tag groups
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SeTagGroup {
    pub title: String,
    uuid: String,
    children: Vec<SeTag>,
    created_date: Option<u64>,
    modified_date: Option<u64>,
    expanded: Option<bool>,
    color: Option<String>,
    textcolor: Option<String>,
}
use std::time::{SystemTime, UNIX_EPOCH};
impl Default for SeTagGroup {
    fn default() -> Self {
        let current_time: Option<u64> = Some(
            SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs(),
        );
        let bg_color = DEFAULT_BG_COLOR.display_rgb();
        let text_color = DEFAULT_TEXT_COLOR.display_rgb();
        let text_hex = format!("{text_color}");
        let bg_hex = format!("{bg_color}");
        Self {
            title: "Default Group".to_string(),
            uuid: Uuid::new_v4().to_string(),
            children: vec![],
            created_date: current_time,
            modified_date: current_time,
            expanded: Some(false),
            color: Some(bg_hex),
            textcolor: Some(text_hex),
        }
    }
}

// Deserialized Tag
#[derive(Serialize, Deserialize, Debug, Clone)]
struct SeTag {
    title: String,
    color: String,
    textcolor: String,
    #[serde(rename = "type")]
    tag_type: Option<String>,
}

impl SeAllTags {
    pub fn update(&mut self, data: &DataCollection) {
        let group_list = data.get_tag_groups_list();
        let original_group_list: Vec<String> =
            self.tag_groups.iter().map(|x| x.title.clone()).collect();
        // First remove deleted groups:
        self.tag_groups = self
            .tag_groups
            .iter()
            .filter(|x| group_list.contains(&x.title))
            .cloned()
            .collect();

        let mut all_groups: Vec<SeTagGroup> = vec![];
        for group in group_list.clone() {
            let mut tmp_group: SeTagGroup = if original_group_list.contains(&group) {
                let old_group: SeTagGroup = self
                    .tag_groups
                    .iter()
                    .filter(|x| x.title == group)
                    .cloned()
                    .collect::<Vec<SeTagGroup>>()
                    .first()
                    .unwrap()
                    .clone();
                SeTagGroup {
                    title: old_group.title,
                    uuid: old_group.uuid,
                    children: vec![],
                    created_date: old_group.created_date,
                    modified_date: old_group.modified_date,
                    expanded: old_group.expanded,
                    color: old_group.color,
                    textcolor: old_group.textcolor,
                }
            } else {
                let tmp_group = data.group_map.get(&group).unwrap();

                let bg_color = Display::new(tmp_group.bg_color.unwrap());
                let text_color = Display::new(tmp_group.text_color.unwrap());
                SeTagGroup {
                    title: group.clone(),
                    color: Some(format!("{bg_color}")),
                    textcolor: Some(format!("{text_color}")),
                    ..Default::default()
                }
            };

            let tags = data.group_tag_map.get(&group);
            if let Some(s1) = tags {
                for tag in s1 {
                    let tmp_tag = data.tag_map.get(tag).unwrap();
                    let bg_color = tmp_tag.bg_color.display_rgb();
                    let text_color = tmp_tag.text_color.display_rgb();
                    let text_hex = format!("{text_color}");
                    let bg_hex = format!("{bg_color}");
                    let se_tag = SeTag {
                        title: tag.to_string(),
                        color: bg_hex,
                        textcolor: text_hex,
                        tag_type: None,
                    };
                    tmp_group.children.push(se_tag);
                }
            }
            all_groups.push(tmp_group);
        }
        self.tag_groups = all_groups
    }
}

/* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
/*                                 Parse Files                                */
/* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum FileType {
    Audio,
    Video,
    Picture,
    Text,
    Web,
    Office,
    Pdf,
}

/// A tuple with the result of the parsing of a filename.
#[derive(Default, Debug, Clone)]
pub struct NameParsingOutput {
    pub artist: Option<String>,
    pub song: String,
    pub tags: Option<Vec<String>>,
    pub extension: Option<String>,
}

fn split_name_to_bracket(s: &str) -> IResult<&str, &str> {
    take_until(0.., "[").parse_peek(s)
}
fn split_name_to_hyphen(s: &str) -> IResult<&str, &str> {
    take_until(0.., " - ").parse_peek(s)
}

fn split_name_ext(s: &str) -> (String, Option<String>) {
    let tmp_res: Vec<&str> = s.split(".").collect();
    let size = tmp_res.len();
    if size == 1 {
        (s.to_string(), None)
    } else if size == 2 {
        // That's (maybe) faster than when there is more than 2 parts. That's
        // should be the most common case
        (tmp_res[0].to_string(), Some(tmp_res[1].to_string()))
    } else {
        (
            tmp_res
                .iter()
                .take(size - 1)
                .map(|x| x.to_string())
                .collect::<Vec<String>>()
                .join("."),
            Some(tmp_res[size - 1].to_owned()),
        )
    }
}

pub fn split_artist_song(s: &str) -> (Option<String>, String) {
    let first_split = split_name_to_hyphen(s);

    match first_split {
        Ok(x) => (
            Some(String::from(x.1.trim())),
            String::from(x.0[3..].trim()),
        ),
        Err(_) => (None, String::from_str(s.trim()).unwrap()),
    }
}

fn parse_tags_extension(input: &str) -> (Option<Vec<String>>, Option<String>) {
    let (song_name, ext) = split_name_ext(input);

    let tags = parse_tags(&song_name);

    (tags, ext)
}

fn split_tags(input: &str) -> Option<Vec<String>> {
    let tags: Vec<String> = input
        .split_ascii_whitespace()
        .collect::<Vec<&str>>()
        .iter()
        .map(|&x| x.to_owned())
        .collect();

    if tags.is_empty() {
        None
    } else {
        Some(tags)
    }
}

/// Parse the part of the string with tags.
///
/// In the case where there is no "]", we consider that there are no tags.
///
///
/// # Arguments
///
/// * `input` - the string to parse

fn parse_tags(input: &str) -> Option<Vec<String>> {
    let res_1 = take::<_, _, InputError<_>>(1usize).parse_peek(input);

    let tags_only = match res_1 {
        Ok(elt) => parse_tags_part2(elt.0),
        Err(_) => None,
    };

    let res: Option<Vec<String>> = match tags_only {
        Some(x) => split_tags(x),
        None => None,
    };
    res
}

/// Parse the part of the string without the first bracket
///
/// Typically, something like: "tag1 tag2]". We check against cases where the second bracket is missing.
///
/// # Arguments
///
/// * `input` - the string to parse
fn parse_tags_part2(input: &str) -> Option<&str> {
    let res_1: std::prelude::v1::Result<(&str, &str), ErrMode<InputError<&str>>> =
        take_until::<_, _, InputError<_>>(0.., "]").parse_peek(input);
    match res_1 {
        Ok(x) => Some(x.1),
        Err(_) => None,
    }
}

fn parse_no_tag_name(input: &str) -> NameParsingOutput {
    let (song_name, extension) = split_name_ext(input);

    let (artist, song) = split_artist_song(&song_name);

    NameParsingOutput {
        artist,
        song,
        tags: None,
        extension,
    }
}

pub fn parse_file_name(input: &str) -> NameParsingOutput {
    let tmp_res: IResult<&str, &str> = split_name_to_bracket(input);
    let res: NameParsingOutput = match tmp_res {
        // There are tags:
        Ok(elt) => {
            let (tags, extension) = parse_tags_extension(elt.0);

            let (artist, song) = split_artist_song(elt.1);
            NameParsingOutput {
                artist,
                song,
                tags,
                extension,
            }
        }
        // There is a file name without tags
        Err(_) => parse_no_tag_name(input),
    };
    res
}

/// Function to parse the JSON file with tags
///
/// # Arguments
///
/// * `tag_file_content` - the content of the JSON file containing the
/// tags.
///
pub fn parse_tag_file(tag_file_content: &str) -> Result<SeAllTags> {
    let res: Result<SeAllTags> = serde_json::from_str(tag_file_content);
    res
}

/// Parse a folder and return all the filenames
///
/// It ignores folders.
///
/// TODO: make it more robust against folder with dot in name
///
/// # Arguments
///
/// * `path` - folder to parse
///
pub fn parse_folder(path: PathBuf) -> Vec<String> {
    let path_string = path.to_str().unwrap().to_owned();
    let format_glob_path = format!("{}/*.*", path_string);
    let mut res: Vec<String> = glob(&format_glob_path)
        .expect("Failed to read glob pattern")
        .filter_map(|entry| match entry {
            Ok(path) => {
                let file_name = path.file_name().unwrap().to_owned().into_string();

                match file_name {
                    Ok(y) => Some(y),
                    Err(_) => None,
                }
            }
            Err(_) => None,
        })
        .collect();
    res.sort();
    res
}

pub fn create_tag_hashmaps(deser_tags: SeAllTags) -> (GroupHashmap, HashMap<String, Tag>) {
    let mut tag_groups: HashMap<String, TagGroup> = HashMap::new();
    let mut tag_map: HashMap<String, Tag> = HashMap::new();

    for tmp_group in &deser_tags.tag_groups {
        for tmp_tag in &tmp_group.children {
            tag_map.insert(
                tmp_tag.title.clone(),
                tag_from_se_tag(tmp_tag.clone(), tmp_group.clone().title.clone()),
            );
        }
        tag_groups.insert(
            tmp_group.title.clone(),
            tag_group_from_se_group(tmp_group.clone()),
        );
    }

    tag_groups.insert(
        DEFAULT_TAG_GROUP.clone(),
        TagGroup {
            title: "Auto tag".into(),
            bg_color: Some(*DEFAULT_BG_COLOR),
            text_color: Some(*DEFAULT_TEXT_COLOR),
        },
    );

    (tag_groups, tag_map)
}

fn tag_group_from_se_group(se_tag_group: SeTagGroup) -> TagGroup {
    let tmp_bg_col = match se_tag_group.color {
        Some(x) => {
            let rgb_from_hex = HexColor::parse(&x).unwrap();
            Some(rgb_from_hex)
        }
        None => None,
    };
    let tmp_text_col = match se_tag_group.textcolor {
        Some(x) => {
            let rgb_from_hex = HexColor::parse(&x).unwrap();
            Some(rgb_from_hex)
        }
        None => None,
    };

    TagGroup {
        title: se_tag_group.title,
        bg_color: tmp_bg_col,
        text_color: tmp_text_col,
    }
}

fn tag_from_se_tag(se_tag: SeTag, group: String) -> Tag {
    let tmp_col = HexColor::parse(&se_tag.color).unwrap();
    let tmp_text_col = HexColor::parse(&se_tag.textcolor).unwrap();

    Tag {
        title: se_tag.title,
        bg_color: tmp_col,
        text_color: tmp_text_col,
        group,
    }
}
