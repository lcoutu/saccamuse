use std::{
    fs::{self},
    path::PathBuf,
    str::FromStr,
};

use crate::data::DataCollection;
use crate::parsing::{create_tag_hashmaps, parse_folder, parse_tag_file};

pub fn parse_folder_script() {
    tracing_subscriber::fmt::init();
    let input = include_str!("./../examples/sample-tags.json");

    let file_tags = parse_tag_file(input).unwrap();

    let (tag_groups, tag_map) = create_tag_hashmaps(file_tags);
    let root_path = PathBuf::from_str("./examples/demo-data").unwrap();
    let mut data_collection = DataCollection::new(root_path.clone());
    data_collection.set_maps(tag_groups, tag_map);

    let folder_to_parse = root_path.clone();

    println!(
        "{} {} {:#?}",
        { "\x1b[38;5;92m⮚⮚⮚⮚⮚⮚\x1b[0m" },
        { "\x1b[38;5;92m Parsing Folder:\x1b[0m" },
        { fs::canonicalize(folder_to_parse.clone()) }
    );
    let file_names = parse_folder(folder_to_parse);

    let now = std::time::Instant::now();
    data_collection.parse_file_names(file_names, root_path);
    let elapsed = now.elapsed();
    println!(
        "{} {} {:#?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92m Tags parsed in:\x1b[0m" },
        { elapsed }
    );
}
