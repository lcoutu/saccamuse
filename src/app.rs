use crate::config::APP_NAME;
use crate::data::DataCollection;
use crate::data::SongEntry;
use crate::data::Tag;
use crate::file_watching::FileWatcher;
use crate::parsing::{
    parse_folder, SeAllTags, DEFAULT_BG_COLOR, DEFAULT_TAG_GROUP, DEFAULT_TEXT_COLOR,
};
use eframe::glow;
use egui::FontData;
use egui::FontDefinitions;
use egui::FontFamily;
use egui::FontFamily::Proportional;
use egui::{
    Color32, Context, FontId, PointerButton, Response, RichText, TextWrapMode, ViewportCommand,
};
use egui::{Pos2, TextStyle::*};

#[cfg(not(target_os = "windows"))]
use libc::{kill, SIGTERM};
#[cfg(target_os = "windows")]
use shlex::try_quote;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::process::Command;

use std::process;
use std::ptr::null_mut;
use std::thread;
use std::time::Duration;
#[cfg(target_os = "windows")]
use winapi::shared::minwindef::DWORD;
#[cfg(target_os = "windows")]
use winapi::shared::ntdef::HANDLE;
#[cfg(target_os = "windows")]
use winapi::um::processthreadsapi::{OpenProcess, TerminateProcess};
#[cfg(target_os = "windows")]
use winapi::um::winnt::{PROCESS_QUERY_INFORMATION, PROCESS_TERMINATE};

use std::ffi::c_void;
use std::ffi::OsStr;
#[cfg(target_os = "windows")]
use std::os::windows::ffi::OsStrExt;
#[cfg(target_os = "windows")]
use std::os::windows::process::CommandExt;
#[cfg(target_os = "windows")]
use winapi::shared::minwindef::LPVOID;
#[cfg(target_os = "windows")]
use winapi::um::handleapi::CloseHandle;
#[cfg(target_os = "windows")]
use winapi::um::synchapi::WaitForSingleObject;
#[cfg(target_os = "windows")]
use winapi::um::winbase::WAIT_OBJECT_0;
#[cfg(target_os = "windows")]
use winapi::um::winuser::PostMessageW;
#[cfg(target_os = "windows")]
use winapi::um::winuser::WM_CLOSE;
// const FONT_SIZE: f32 = 18.;
const TAG_EDITOR_ROW_NB: usize = 5;

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct SaccamuseApp {
    file_view: FileView,
    #[serde(skip)]
    file_watcher: Option<FileWatcher>,
    #[serde(skip)]
    data: DataCollection,
    app_state: AppState,
    #[serde(skip)]
    tag_editor: Option<TagEditor>,
    #[serde(skip)]
    tag_file: Option<SeAllTags>,
    #[serde(skip)]
    tag_file_path: Option<PathBuf>,
    #[serde(skip)]
    song_filter: String,
}

impl Default for SaccamuseApp {
    fn default() -> Self {
        Self {
            // Example stuff:
            file_view: FileView::default(),
            file_watcher: None,
            app_state: AppState::default(),
            data: DataCollection::default(),
            tag_editor: None,
            tag_file: None,
            tag_file_path: None,
            song_filter: "".to_string(),
        }
    }
}

#[derive(Default, Debug, Clone, serde::Serialize, serde::Deserialize, PartialEq)]
enum SortByEnum {
    #[default]
    Name,
    BPM,
}

#[derive(Default, Debug, Clone, serde::Deserialize, serde::Serialize)]
#[serde(default)]
struct AppState {
    edited_song: Option<String>,
    player_process: Option<u32>,
    selected_tag_group: Option<String>,
    sort_by: SortByEnum,
}

/* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
/*                                  Impl App                                  */
/* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */

impl SaccamuseApp {
    /// Called once before the first frame.
    pub fn new(
        cc: &eframe::CreationContext<'_>,
        data_collection: DataCollection,
        file_watcher: FileWatcher,
        tag_file: SeAllTags,
        tag_file_path: PathBuf,
    ) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.
        set_font_style(&cc.egui_ctx);

        let mut app_state: AppState = AppState::default();
        if let Some(storage) = cc.storage {
            if let Some(state) = eframe::get_value(storage, &APP_NAME) {
                app_state = state;
            }
        }

        Self {
            file_view: FileView {},
            app_state,
            file_watcher: Some(file_watcher),
            data: data_collection,
            tag_editor: None,
            tag_file: Some(tag_file),
            tag_file_path: Some(tag_file_path),
            ..Default::default()
        }
    }
    pub fn process_tag_file(&mut self, mut tag_file: SeAllTags) {
        tag_file.update(&self.data);
        let json_str = serde_json::to_string_pretty(&tag_file).unwrap();

        let mut file = File::create(self.tag_file_path.clone().unwrap()).unwrap();

        let res = file.write_all(json_str.as_bytes());
        match res {
            Ok(_) => (),
            Err(s1) => {
                log::warn!("{s1}")
            }
        }
        self.tag_file = Some(tag_file);
    }
}

fn set_font_style(ctx: &egui::Context) {
    let mut fonts = FontDefinitions::default();

    fonts.font_data.insert(
        String::from("lunasima"),
        FontData::from_static(include_bytes!("../assets/Lunasima-Regular.ttf")),
    );

    fonts
        .families
        .get_mut(&FontFamily::Proportional)
        .unwrap()
        .push("lunasima".to_owned());

    ctx.set_fonts(fonts);

    let text_styles: BTreeMap<_, _> = [
        (Heading, FontId::new(30.0, Proportional)),
        (Name("Heading2".into()), FontId::new(25.0, Proportional)),
        (Name("Context".into()), FontId::new(23.0, Proportional)),
        (Body, FontId::new(20.0, Proportional)),
        (Monospace, FontId::new(14.0, Proportional)),
        (Button, FontId::new(14.0, Proportional)),
        (Small, FontId::new(10.0, Proportional)),
    ]
    .into();

    // Mutate global styles with new text styles
    ctx.all_styles_mut(move |style| style.text_styles = text_styles.clone());
}
impl eframe::App for SaccamuseApp {
    /// Called by the frame work to save state before shutdown.
    // fn save(&mut self, storage: &mut dyn eframe::Storage) {
    //     eframe::set_value(storage, eframe::APP_KEY, self);
    // }

    /// Called each time the UI needs repainting, which may be many times per second.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        // Check if some files have been modified:
        let mut table_has_been_updated: bool = false;
        if let Some(x) = &self.file_watcher {
            let file_modified = x.new_change.lock();
            match file_modified {
                Ok(mut y) => {
                    if *y {
                        table_has_been_updated = true;

                        let file_names = parse_folder(x.path.clone());

                        self.data.parse_file_names(file_names, x.path.clone());
                    }
                    *y = false;
                    drop(y);
                }
                Err(_) => log::warn!("Failed to acquire lock"),
            }
        }

        // Put your widgets into a `SidePanel`, `TopBottomPanel`, `CentralPanel`, `Window` or `Area`.
        // For inspiration and more examples, go to https://emilk.github.io/egui

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:

            egui::menu::bar(ui, |ui| {
                // NOTE: no File->Quit on web pages!

                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        ctx.send_viewport_cmd(egui::ViewportCommand::Close);
                    }
                });
                ui.add_space(16.0);

                egui::widgets::global_theme_preference_buttons(ui);
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            // The central panel the region left after adding TopPanel's and SidePanel's
            let folder_path: &str = self.data.root_path.to_str().unwrap();

            let click_on_root_folder: Option<Response> = Some(ui.heading(folder_path));

            let click_on_root_folder_bool: bool = match click_on_root_folder {
                Some(x) => x.double_clicked(),
                None => false,
            };
            if click_on_root_folder_bool {
                open_folder(self.data.root_path.clone());
            }

            ui.separator();
            ui.horizontal(|ui| {
                ui.label("Sort by:");
                ui.radio_value(&mut self.app_state.sort_by, SortByEnum::Name, "Name");
                ui.radio_value(&mut self.app_state.sort_by, SortByEnum::BPM, "BPM");
                ui.add(
                    egui::TextEdit::singleline(&mut self.song_filter).hint_text("Filter songs..."),
                );
                if ui.button("Clear Filter").clicked() {
                    self.song_filter = "".to_string();
                }
            });
            ui.separator();

            let tmp_filter_song: Option<String> = if self.song_filter.is_empty() {
                None
            } else {
                Some(self.song_filter.clone())
            };

            self.app_state = self.file_view.ui(
                ui,
                ctx,
                table_has_been_updated,
                self.app_state.sort_by.clone(),
                &self.data,
                &self.app_state,
                tmp_filter_song,
            );
            ui.separator();

            ui.with_layout(egui::Layout::bottom_up(egui::Align::LEFT), |ui| {
                // powered_by_egui_and_eframe(ui);
                egui::warn_if_debug_build(ui);
            });
        });

        self.update_tag_editor(ctx)
    }

    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, &APP_NAME, &self.app_state);
    }

    fn on_exit(&mut self, _gl: Option<&glow::Context>) {
        let file_watcher = self.file_watcher.take();
        if let Some(mut x) = file_watcher {
            x.stop_watcher()
        };
        self.app_state.edited_song = None;
        self.app_state.player_process = None;
    }
}

impl SaccamuseApp {
    fn update_tag_editor(&mut self, ctx: &egui::Context) {
        if let Some(x) = self.app_state.edited_song.clone() {
            if self.tag_editor.is_none() {
                let tmp_song = self.data.song_entries.get(&x).unwrap().clone();
                let mut tag_groups = self.data.get_tag_groups_list();
                tag_groups.sort_by_key(|a| a.to_lowercase());
                let song_name_edit = tmp_song.full_name();
                let bg_color = DEFAULT_BG_COLOR.display_rgb();
                let text_color = DEFAULT_TEXT_COLOR.display_rgb();
                let text_hex = format!("{text_color}");
                let bg_hex = format!("{bg_color}");
                let mut selected_group: usize = 0;
                if let Some(s1) = &self.app_state.selected_tag_group {
                    let mut j = 0;
                    for elt in tag_groups.iter() {
                        if *elt == *s1 {
                            selected_group = j;
                            break;
                        }
                        if *s1 != *DEFAULT_TAG_GROUP {
                            j += 1;
                        }
                    }
                }

                self.tag_editor = Some(TagEditor {
                    song: tmp_song,
                    output: None,
                    song_name_edit,
                    finished: false,
                    current_state: SongEntryModifs::default(),
                    new_tag_group_box_edit: "".to_string(),
                    tag_groups,
                    selected_group,
                    root_path: self.data.root_path.clone(),
                    group_text_color: Color32::from_hex(&text_hex).unwrap(),
                    group_bg_color: Color32::from_hex(&bg_hex).unwrap(),
                    tag_file: self.tag_file.take(),
                })
            }
        }

        if let Some(mut tag_editor) = self.tag_editor.clone() {
            ctx.show_viewport_immediate(
                egui::ViewportId::from_hash_of("immediate_viewport"),
                egui::ViewportBuilder::default()
                    .with_title("Edit tags and song name")
                    .with_inner_size([1000.0, 400.0])
                    .with_always_on_top()
                    .with_position(Pos2::new(500., 500.)),
                |ctx, class| {
                    assert!(
                        class == egui::ViewportClass::Immediate,
                        "This egui backend doesn't support multiple viewports"
                    );

                    tag_editor.ui(ctx, &mut self.data);

                    if ctx.input(|i| i.viewport().close_requested()) {
                        if let Some(s1) = &self.tag_editor {
                            self.app_state.selected_tag_group =
                                Some(s1.tag_groups[s1.selected_group].clone());
                        }
                        // Tell parent viewport that we should not show next frame:
                        self.app_state.edited_song = None;
                        self.tag_editor = None
                    }
                },
            );

            if self.app_state.edited_song.is_some() {
                self.tag_editor = Some(tag_editor);
            } else {
                // Stop VLC before renaming songs
                if let Some(x) = self.app_state.player_process {
                    kill_process(x as i32);
                }
                // If we stop the editing, we delete the tag editor from the app
                // and process the copy
                let updated_tag_file = tag_editor.tag_file.take().unwrap();
                self.process_tag_file(updated_tag_file);
                self.data.process_tag_editor(tag_editor);
                self.tag_editor = None;
            }
        }
    }
}

/* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
/*                                  TagEditor                                 */
/* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */

#[derive(Default, Debug, Clone)]
pub struct SongEntryModifs {
    pub tags: Option<Vec<String>>,
    pub full_song_name: Option<String>,
    pub delete_song: bool,
}

#[derive(Debug, Clone)]
pub struct TagEditor {
    pub song: SongEntry,
    finished: bool,
    current_state: SongEntryModifs,
    pub output: Option<SongEntryModifs>,
    pub song_name_edit: String,
    pub new_tag_group_box_edit: String,
    tag_groups: Vec<String>,
    selected_group: usize,
    root_path: PathBuf,
    group_text_color: Color32,
    group_bg_color: Color32,
    tag_file: Option<SeAllTags>,
}

impl TagEditor {
    pub fn ui(&mut self, ctx: &Context, data: &mut DataCollection) {
        let mut tags_to_remove: Vec<String> = vec![];

        data.sort_tags();

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical(|ui| {
                ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
                    ui.vertical(|ui| {
                        ui.horizontal(|ui| {
                            ui.add(
                                egui::TextEdit::singleline(&mut self.song_name_edit)
                                    .hint_text("Song name"),
                            );

                            ui.horizontal(|ui| {
                                if let Some(tmp_tags) = self.get_tags() {
                                    for tmp_tag in tmp_tags {
                                        let tag_opt = data.tag_map.get(&tmp_tag);
                                        let tag = match tag_opt {
                                            Some(s1) => s1,
                                            None => {
                                                data.add_tag(
                                                    tmp_tag.clone(),
                                                    Some(&self.tag_groups[self.selected_group]),
                                                );
                                                data.tag_map.get(&tmp_tag).unwrap()
                                            }
                                        };
                                        let text_col = tag.text_color;
                                        let bg_col = tag.bg_color;
                                        let name = tag.title.clone();
                                        let click_on_tags: Option<Response> = Some(
                                            ui.label(
                                                RichText::new(name)
                                                    .color(Color32::from_rgb(
                                                        text_col.r, text_col.g, text_col.b,
                                                    ))
                                                    .background_color(Color32::from_rgb(
                                                        bg_col.r, bg_col.g, bg_col.b,
                                                    )),
                                            ),
                                        );
                                        if let Some(x) = click_on_tags {
                                            match x.clicked_by(PointerButton::Primary) {
                                                true => self.move_tag(&tmp_tag, true),
                                                false => (),
                                            }
                                            match x.clicked_by(PointerButton::Secondary) {
                                                true => self.move_tag(&tmp_tag, false),
                                                false => (),
                                            }
                                            match x.clicked_by(PointerButton::Middle) {
                                                true => tags_to_remove.push(tmp_tag.clone()),
                                                false => (),
                                            }
                                        };
                                    }
                                }
                            });
                        });
                        ui.horizontal(|ui| {
                            ui.add(
                                egui::TextEdit::singleline(&mut self.new_tag_group_box_edit)
                                    .hint_text("New tag/group"),
                            );

                            ui.vertical(|ui| {
                                ui.horizontal(|ui| {
                                    egui::ComboBox::from_label("")
                                        .selected_text(&self.tag_groups[self.selected_group])
                                        .show_ui(ui, |ui| {
                                            for i in 0..self.tag_groups.len() {
                                                if self.tag_groups[i] != *DEFAULT_TAG_GROUP {
                                                    let value = ui.selectable_value(
                                                        &mut &self.tag_groups[i],
                                                        &self.tag_groups[self.selected_group],
                                                        &self.tag_groups[i],
                                                    );
                                                    if value.clicked() {
                                                        self.selected_group = i;
                                                    }
                                                }
                                            }
                                        });

                                    if ui.button("Add tag").clicked() {
                                        data.add_tag(
                                            self.new_tag_group_box_edit.clone(),
                                            Some(&self.tag_groups[self.selected_group]),
                                        );
                                        self.add_tag();
                                        self.new_tag_group_box_edit = "".to_string();
                                    }
                                    if ui.button("Del. tag from database").clicked() {
                                        data.remove_tag(self.new_tag_group_box_edit.clone());
                                        let file_names = parse_folder(self.root_path.clone());
                                        data.parse_file_names(file_names, self.root_path.clone());
                                        self.new_tag_group_box_edit = "".to_string();
                                    }
                                });

                                ui.horizontal(|ui| {
                                    ui.color_edit_button_srgba(&mut self.group_text_color)
                                        .on_hover_text("Text color");
                                    ui.color_edit_button_srgba(&mut self.group_bg_color)
                                        .on_hover_text("Background color");
                                    if ui.button("Add group").clicked() {
                                        let group_exists = data.add_group(
                                            self.new_tag_group_box_edit.clone(),
                                            self.group_text_color,
                                            self.group_bg_color,
                                        );

                                        if !group_exists {
                                            self.tag_groups
                                                .push(self.new_tag_group_box_edit.clone());
                                        }
                                        self.new_tag_group_box_edit = "".to_string();

                                        let mut tmp_tag_file = self.tag_file.take().unwrap();
                                        tmp_tag_file.update(data);
                                        self.tag_file = Some(tmp_tag_file);
                                    }
                                    if ui.button("Delete group").clicked() {
                                        let group_exists =
                                            data.delete_group(self.new_tag_group_box_edit.clone());
                                        let file_names = parse_folder(self.root_path.clone());
                                        data.parse_file_names(file_names, self.root_path.clone());
                                        if group_exists {
                                            self.tag_groups = self
                                                .tag_groups
                                                .iter()
                                                .filter(|x| {
                                                    **x != self.new_tag_group_box_edit.clone()
                                                })
                                                .cloned()
                                                .collect::<Vec<String>>();
                                            if self.tag_groups.is_empty() {
                                                // Re-add the default tag group to the list:
                                                self.tag_groups =
                                                    vec![DEFAULT_TAG_GROUP.to_string()];
                                                self.selected_group = 0;
                                            } else {
                                                // Take the first non-default tag group
                                                self.selected_group = self
                                                    .tag_groups
                                                    .iter()
                                                    .position(|x| **x != *DEFAULT_TAG_GROUP)
                                                    .unwrap_or(0);
                                            }
                                        }
                                        self.new_tag_group_box_edit = "".to_string();
                                    }
                                })
                            })
                        });
                    });
                });

                let group_nb = self.tag_groups.len();
                let col_nb = group_nb / TAG_EDITOR_ROW_NB + 1;
                egui::Grid::new("All tags selection")
                    .striped(true)
                    .show(ui, |ui| {
                        let mut tot_nb: usize = 0;
                        let tag_groups = self.tag_groups.clone();

                        for _row in 0..TAG_EDITOR_ROW_NB {
                            for _col in 0..col_nb {
                                if tot_nb >= tag_groups.len() {
                                    break;
                                }

                                ui.push_id(tot_nb, |ui| {
                                    egui::ComboBox::from_label("")
                                        .selected_text(tag_groups[tot_nb].clone())
                                        .show_ui(ui, |ui| {
                                            let tags: &Vec<String> = data
                                                .group_tag_map
                                                .get(&tag_groups[tot_nb])
                                                .unwrap();

                                            let tag_nb = tags.len();
                                            (0..tag_nb).for_each(|i| {
                                                let value = ui.selectable_value(
                                                    &mut Some(tags[i].clone()),
                                                    None,
                                                    &tags[i],
                                                );
                                                if value.clicked() {
                                                    self.new_tag_group_box_edit = tags[i].clone();
                                                    self.add_tag();
                                                    self.new_tag_group_box_edit = "".to_string();
                                                }
                                            });
                                        });
                                });
                                tot_nb += 1;
                            }
                            ui.end_row();
                        }
                    })
            })
        });

        /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
        /*                               Process                               */
        /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
        self.process_changes(tags_to_remove);
        // let mut output = TagEditorResults::default();

        /* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ Save and cancel ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
        egui::TopBottomPanel::bottom("buttons").show(ctx, |ui| {
            ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
                if ui.button("Save").clicked() {
                    ctx.send_viewport_cmd(ViewportCommand::Close);
                    self.finished = true;
                    // Let's clone the final result as output of the editor:
                    self.output = Some(self.current_state.clone());
                }
                if ui.button("Cancel").clicked() {
                    ctx.send_viewport_cmd(ViewportCommand::Close);
                    self.output = None;
                    self.finished = true;
                }
                ui.checkbox(&mut self.current_state.delete_song, "Delete Song");
            })
        });
    }

    fn process_changes(&mut self, tags_to_remove: Vec<String>) {
        // Removes tags (nothing to do if no tags in song):
        if !tags_to_remove.is_empty() {
            if let Some(s1) = self.get_tags() {
                let filtered_tags: Vec<String> = s1
                    .iter()
                    .filter(|v1| !tags_to_remove.contains(v1))
                    .cloned()
                    .collect();
                self.current_state.tags = Some(filtered_tags);
            }
        }

        // Check if there are changes in the name:
        if self.song_name_edit != self.song.full_name() {
            self.current_state.full_song_name = Some(self.song_name_edit.clone());
        }
    }

    /// Get the tags with the updates
    fn get_tags(&self) -> Option<Vec<String>> {
        match self.current_state.tags.clone() {
            Some(s1) => Some(s1),
            None => self.song.tags.clone(),
        }
    }

    fn add_tag(&mut self) {
        // Check if we have already modified the tags:
        if let Some(s1) = self.current_state.tags.clone() {
            let mut existing_tags = s1;
            // Check that the tag does not exist in the song name
            if existing_tags
                .iter()
                .any(|x| *x == *self.new_tag_group_box_edit)
            {
                return;
            }
            existing_tags.push(self.new_tag_group_box_edit.clone());
            self.current_state.tags = Some(existing_tags);
        } else {
            let existing_tags = self.song.tags.clone();
            let mut updated_tags: Vec<String> = vec![];
            if let Some(s2) = existing_tags {
                updated_tags = [updated_tags, s2].concat();
            }
            // Check that the tag does not exist in the song name
            if updated_tags
                .iter()
                .any(|x| *x == *self.new_tag_group_box_edit)
            {
                return;
            }
            updated_tags.push(self.new_tag_group_box_edit.clone());
            self.current_state.tags = Some(updated_tags);
        }
    }

    fn move_tag(&mut self, tag: &String, is_direction_left: bool) {
        // If there is no tag, there is no way one could have clicked on a tag,
        // so just ignore it.
        if let Some(current_tags) = self.get_tags() {
            let index = current_tags.iter().position(|x| *x == *tag);
            if let Some(i) = index {
                let mut updated_tags = current_tags.clone();
                if is_direction_left {
                    if i == 0 {
                        return;
                    };
                    updated_tags.swap(i, i - 1);
                    self.current_state.tags = Some(updated_tags);
                } else {
                    if i == (current_tags.len() - 1) {
                        return;
                    };
                    updated_tags.swap(i, i + 1);
                    self.current_state.tags = Some(updated_tags);
                }
            }
        }
    }
}
/* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */
/*                                  FileView                                  */
/* ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ */

#[derive(Default, serde::Deserialize, serde::Serialize)]
#[serde(default)]
struct FileView {}

impl FileView {
    pub fn ui(
        &mut self,
        ui: &mut egui::Ui,
        _ctx: &Context,
        reset_table: bool,
        sort_by: SortByEnum,
        data: &DataCollection,
        app_state: &AppState,
        song_filter: Option<String>,
    ) -> AppState {
        use egui_extras::{Size, StripBuilder};
        let mut updated_app_state: AppState = app_state.clone();
        StripBuilder::new(ui)
            .size(Size::remainder().at_least(100.0)) // for the table
            .vertical(|mut strip| {
                strip.cell(|ui| {
                    egui::ScrollArea::horizontal().show(ui, |ui| {
                        updated_app_state =
                            self.table_ui(ui, reset_table, data, sort_by, app_state, song_filter);
                    });
                });
            });
        updated_app_state
    }

    fn table_ui(
        &mut self,
        ui: &mut egui::Ui,
        reset_table: bool,
        data: &DataCollection,
        sort_by: SortByEnum,
        app_state: &AppState,
        song_filter: Option<String>,
    ) -> AppState {
        use egui_extras::{Column, TableBuilder};

        let text_height = egui::TextStyle::Body
            .resolve(ui.style())
            .size
            .max(ui.spacing().interact_size.y);

        let available_height = ui.available_height();
        let mut table = TableBuilder::new(ui)
            .striped(true)
            .resizable(true)
            .cell_layout(egui::Layout::left_to_right(egui::Align::Center))
            .column(Column::auto())
            .column(Column::auto())
            .column(Column::remainder())
            .min_scrolled_height(0.0)
            .max_scroll_height(available_height);

        let mut updated_app_state = app_state.clone();
        table = table.sense(egui::Sense::click());
        if reset_table {
            table.reset()
        }
        let mut clicked_tags_song: Option<String> = None;
        let mut filtered_songs: Vec<String> = vec![];
        let tmp_data_len = match song_filter.clone() {
            Some(s1) => match sort_by {
                SortByEnum::Name => {
                    filtered_songs = data.filtered_row_entries(s1);
                    filtered_songs.len()
                }
                SortByEnum::BPM => {
                    filtered_songs = data.filtered_row_entries_by_bpm(s1);
                    filtered_songs.len()
                }
            },
            None => data.row_entries.len(),
        };

        table
            .header(20.0, |mut header| {
                header.col(|ui| {
                    ui.strong("");
                });
                header.col(|ui| {
                    ui.strong("Files");
                });
                header.col(|ui| {
                    ui.strong("Tags");
                });
            })
            .body(|body| {
                body.rows(text_height, tmp_data_len, |mut row| {
                    let i = row.index();
                    let file_entry_name: Option<&String> = match song_filter {
                        Some(_) => filtered_songs.get(i),
                        None => match sort_by {
                            SortByEnum::Name => data.row_entries.get(&i),
                            SortByEnum::BPM => data.row_entries_by_bpm.get(&i),
                        },
                    };

                    row.col(|ui| {
                        // let file_entry_name: Option<&String> = data.row_entries.get(&i);

                        if let Some(s1) = file_entry_name {
                            let song_path =
                                data.song_entries.get(s1).unwrap().clone().full_path.clone();
                            if ui.button("Enqueue").clicked() {
                                add_song_to_music_player(song_path);
                            }
                        }
                    });
                    row.col(|ui| {
                        // let file_entry_name: Option<&String> = data.row_entries.get(&i);

                        if let Some(x) = file_entry_name {
                            let file_entry = data.song_entries.get(x).unwrap();
                            let click_on_song_name_1 = ui.add(
                                egui::Label::new(match &file_entry.artist {
                                    Some(x) => RichText::new(x)
                                        .color(Color32::from_rgb(255, 230, 165))
                                        .background_color(Color32::from_rgb(96, 86, 120)),
                                    None => file_entry.song_name.clone().into(),
                                })
                                .wrap_mode(TextWrapMode::Extend), //.sense(egui::Sense::hover())
                            );

                            let mut click_on_song_name: Option<Response> = None;
                            if file_entry.artist.is_some() {
                                click_on_song_name = Some(
                                    ui.add(
                                        egui::Label::new(&file_entry.song_name)
                                            .wrap_mode(TextWrapMode::Extend), //.sense(egui::Sense::hover())
                                    ),
                                );
                            }

                            let click_on_song_name_bool: bool = match click_on_song_name {
                                Some(x) => x.double_clicked(),
                                None => false,
                            };
                            if click_on_song_name_bool || click_on_song_name_1.double_clicked() {
                                if let Some(x) = updated_app_state.player_process {
                                    kill_process(x as i32);
                                    updated_app_state.player_process = None
                                }
                                updated_app_state.player_process =
                                    play_song(data.song_entries.get(x).unwrap().clone());
                            }
                        }
                    });
                    row.col(|ui| {
                        // let file_entry_name = data.row_entries.get(&i);

                        if let Some(x) = file_entry_name {
                            let file_entry = data.song_entries.get(x).unwrap();
                            if let Some(x) = file_entry.tags.clone() {
                                for tmp_tag in x.iter() {
                                    let tag = data.tag_map.get(tmp_tag);
                                    let tag = match tag {
                                        Some(s1) => s1,
                                        None => &{
                                            Tag {
                                                title: tmp_tag.into(),
                                                ..Default::default()
                                            }
                                        },
                                    };
                                    let text_col = tag.text_color;
                                    let bg_col = tag.bg_color;
                                    let name = tag.title.clone();
                                    let click_on_tags = Some(
                                        ui.label(
                                            RichText::new(name)
                                                .color(Color32::from_rgb(
                                                    text_col.r, text_col.g, text_col.b,
                                                ))
                                                .background_color(Color32::from_rgb(
                                                    bg_col.r, bg_col.g, bg_col.b,
                                                )),
                                        ),
                                    );
                                    if let Some(x) = click_on_tags {
                                        match x.double_clicked() {
                                            true => {
                                                clicked_tags_song = Some(file_entry.hash_string())
                                            }
                                            false => (),
                                        }
                                    };
                                }

                                if x.is_empty() && ui.button("Edit").clicked() {
                                    clicked_tags_song = Some(file_entry.hash_string());
                                }
                            }
                        }
                    });
                    if let Some(x) = clicked_tags_song.clone() {
                        updated_app_state.edited_song = Some(x);
                    }
                })
            });
        updated_app_state
    }
}

fn play_song(file: SongEntry) -> Option<u32> {
    let path = file.full_path;

    let child = {
        #[cfg(target_os = "windows")]
        {
            let vlc_comm = r#"C:\Program Files (x86)\VideoLAN\VLC\vlc.exe"#;
            let esc_lc_comm = try_quote(vlc_comm).unwrap().into_owned();
            let file_name = path.to_str().unwrap().to_string();
            let esc_file_name = try_quote(&file_name).unwrap().into_owned();

            Command::new("powershell")
                .args(["&", &esc_lc_comm, &esc_file_name])
                .creation_flags(0x08000000)
                .spawn()
        }

        #[cfg(not(target_os = "windows"))]
        {
            Command::new("vlc").arg(path.to_str().unwrap()).spawn()
        }
    };

    match child {
        Ok(x) => Some(x.id()),
        Err(_) => {
            println!("Cannot open: {} ", { path.to_str().unwrap() });
            None
        }
    }
}

fn kill_process(pid: i32) {
    #[cfg(not(target_os = "windows"))]
    {
        unsafe {
            kill(pid, SIGTERM);
        };
    }
    #[cfg(target_os = "windows")]
    {
        let tmp_pid: i32;
        // This is a hack because on Windows, the PID is the PID of the powershell,
        // not the PID of VLC:
        unsafe {
            let list = tasklist::tasklist();
            let mut tmp_pid_2: Option<&u32> = None;

            for elt in list.keys() {
                let trimmed_key = elt.trim_matches(|c: char| c.is_whitespace() || c == '\0');
                println!(
                    "{:#?} {:#?}",
                    trimmed_key.clone(),
                    *trimmed_key == "vlc.exe".to_string()
                );
                if *trimmed_key == "vlc.exe".to_string() {
                    tmp_pid_2 = list.get(elt);
                }
            }

            if let Some(i) = tmp_pid_2 {
                tmp_pid = *i as i32;
            } else {
                println!("Could not find the VLC process.");
                return;
            }
        }
        let h_process = unsafe { OpenProcess(PROCESS_TERMINATE, false as i32, tmp_pid as u32) };
        if h_process.is_null() {
            println!("Failed to open process with PID: {}", tmp_pid);
            return;
        }

        // Terminate the process
        let exit_code = 0;
        let result = unsafe { TerminateProcess(h_process, exit_code) };
        if result == 0 {
            println!("Failed to terminate process with PID: {}", tmp_pid);
        } else {
            println!("Terminated process with PID: {}", tmp_pid);
        }

        // Close the process handle
        unsafe { CloseHandle(h_process) };
    }
}

fn add_song_to_music_player(song_path: PathBuf) {
    #[cfg(not(target_os = "windows"))]
    {
        let binding = song_path.into_os_string();
        let song_path_string = binding.to_str().unwrap();

        let _ = Command::new("clementine")
            .arg("--append")
            // .arg("--play-pause")
            .arg(song_path_string)
            .output()
            .expect("failed to execute process");
    }
    #[cfg(target_os = "windows")]
    println!(
        "{} {} {:#?}",
        { "\x1b[38;5;92m➤➤➤\x1b[0m" },
        { "\x1b[38;5;92m :\x1b[0m" },
        { "Adding to playlist not supported on Windows yet." }
    );
}

fn open_folder(path: PathBuf) {
    let _ = opener::open(path.as_os_str().to_str().unwrap());
}
